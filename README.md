# `Shenzhen I/O` Solitaire Solver Bot

This project provides a bot implemented in `Rust`, which is able to solve
the Solitaire game in
[`Shenzhen I/O`](https://www.zachtronics.com/shenzhen-io/)
by **Zachtronics**. \
_(The [solitaire](https://zachtronics.itch.io/shenzhen-solitaire)
also exists as a standalone game)_.

![Shenzhen I/O Logo](https://img.itch.zone/aW1nLzIyNzAzNjIucG5n/original/Wg8ISy.png){width=500px}
![Shenzhen I/O Solitaire game](https://img.itch.zone/aW1hZ2UvNDQ4OTM0LzIyNzAzNzMuanBn/original/8KECkd.jpg){width=500px}

This bot was inspired by
[this python solver](https://github.com/emichael/shenzhen-solitaire-bot)
especially for the game rules implementation.

When started, the bot is told to solve a certain number of games.
You should have started a solitaire game at this point. The program will
then take a screenshot of the active window and try to identify the game board.
Once identified, it will try to solve it until a timeout is reached.
When the solution is found, the bot takes control of the mouse and plays all moves
needed to clear the board. \
If the board cannot be identified or solved (and also if there are still
some games to solve), the bot will automatically click on 'new game'.

### Disclaimer

If you want a go-to solution to solve the solitaire game, check the
[the python solver](https://github.com/emichael/shenzhen-solitaire-bot) project.
It is probably way easier to make it run right away.

This project was a pretext for me to write some consistent `Rust` to become more
fluent with it. It suits my needs, but it has not been tested on lots of configurations.
For instance, the screen resolution seems to impact a lot the template matching for recognizing
the cards symbols. Therefore, the code has been set up with a resolution of `1920x1080`
which is quite common.\
So, if you really want to use it, be ready to do some `Rust` coding to adapt some code sections.
(Do not hesitate to open an issue to upgrade this project, see the `Contributing` section below).

As one of my first `Rust` project, there might be some places where the code isn't
idiomatic and/or well written. If some wise Rustacean can kindly highlight these to me,
I will be pleased to improve the code regarding `Rust` guidelines and learn some more
(as long as you don't question the entire codebase).

## Building

This project contains 2 features to select either `imageproc` or `opencv` libraries for
image processing (identifying the game board from a screenshot).
 * [`opencv`](https://crates.io/crates/opencv)
 (legacy): I started with `opencv` because I already used it in the past.
 * [`imageproc`](https://crates.io/crates/imageproc)
 (recommended): The `imageproc` library is implemented in Rust and I should
    have noticed it before starting.

The default feature uses the `imageproc` library, in order to use `opencv` instead,
it is best to disable default features and select `opencv`:

> `cargo build --no-default-features --features=opencv`

The `opencv` build adds some dependencies and complexifies the building process. It may sometimes
be a bit cumbersome to have it properly built (check the
[opencv crate page](https://crates.io/crates/opencv) for instructions). \
That is why the project has switched towards `imageproc` which is implemented in `Rust`.

**In both cases, make sure to compile on `release` mode.**

> `cargo build --relase`

## Usage

As said before, this project ~~works on my machine~~ suits my needs but has not been tested on
lots of configurations.

_The project is tested on Linux, I don't know for Windows. The screen resolution can also
introduce issues. The project tries to be resolution independent, but it hasn't been tested in
on a lot of resolutions and you never know._

### Multiple monitors

_These instructions concern Linux `X11`.
It is still not tested for other configurations, nor for Windows._

When using multiple monitors, you should supply the one on which to take the screenshot
using the `-s` argument. The resulting screenshot can be checked using the `-z` option
(see below).

If you are using multiple monitors, the one that runs the game should be the top-left one.

![Correct monitor positionning on dual screen](imgs/dualscreen_position.png){width=500px}

_Supposing the game window is on the screen `1`_

This is because mouse coordinates are global for all monitors on `X11` (with `libxdo` which is used
by the bot to click with the mouse), and the bot has no way to know where the game window is located.

**For the same reason, the game should be played fullscreen for the bot to work.**

_If you want to see the terminal while solving with only one monitor, here is my tip:_ \
_(Set the terminal window to 'Always on top' and reduce font size)._

![Little terminal over the game with single screen](imgs/single_screen_tip.png){width=600px}

This method doesn't prevent the bot to catch the terminal instead of a card sometimes...

### CLI arguments

The program accepts some command line arguments to tweak its behaviour.

If you do not give it a number of solves as a parameter, it will only solve one.

A message asking you to press `Enter` when you are ready can be skipped using the `--start` option.

_Unfortunately, the only way to stop the bot if things go wrong is to `Alt-Tab` - `Ctrl-C`
fast enough. I originally wanted to provide a way to communicate with the bot through
keystrokes, but there was no easy nor protable way to do this, considering the focus
is on the game and not on the bot window._

```
$ ./shenzhen_io_solitaire_solver -h
Shenzhen I/O Solitaire Solver Bot 0.9.2
Wirefl0w
A Rust Solver Bot for the Shenzhen I/O Solitaire game

USAGE:
    shenzhen_io_solitaire_solver_bot [FLAGS] [OPTIONS] [nb solves]

FLAGS:
    -h, --help          Prints help information
    -z, --show_zones    Without playing, displays detected board and saves a png image with the detection/click zones
                        drawed at './sio_bot_zones.png'.
        --start         Start the solver without asking to press Enter
    -V, --version       Prints version information

OPTIONS:
    -s, --screen <screen>      The screen to take a screenshot from (if multiple monitors are connected [default: 0]
    -t, --timeout <timeout>    The timeout (in ms) at which the solver will consider the game not solvable. [default:
                               5000]

ARGS:
    <nb solves>    The number of game the solver will solve. (Max 255) [default: 1]
```

If you run it with `cargo`:
> cargo run --release -- \<your options\>

The `-z` option saves an image showing the different areas. The red rectangles show areas where
the template matching for card recognition occurs. Green dots show other areas where the bot
might want to click.

![Image with detection and click areas drawned](imgs/sio_bot_zones.png){width=500px}

This option is usefull to check is the right screen is used for the screenshot, or for
debugging purposes.

## Contributing

As said, this project may lack some compatibility / features and has some room for improvement.

The project may not be in active developpement (as a somewhat specific application), but
if you'd like to contribute, feel free to do so, whether you want to add new features, improve
compatibility with different configurations or highlight a piece of code which isn't Rust-idiomatic.

I cannot guarante to be very fast to answer to opened issues or pull requests but I would be
pleased to see improvements integrated in this repo. (Anyway, you are free to fork the project
and bring all the changes you want on your own).

It would be great to stick with Rust idioms and good practices as much as possible.

### Project Structure

The project is divided into a module containing all the game rules implementation,
a module doing the image processing and the bot part, responsible of taking the screenshot,
and clicking with the mouse.

Everything is documented, so you can fire `cargo doc` and you should find everything you need.
