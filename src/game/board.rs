//
// Copyright (C) 2021, 2023-2024  Florent "Wirefl0w" C.
//
// This file is part of shenzhen_io_solitaire_solver_bot
//
// shenzhen_io_solitaire_solver_bot is free software:
// you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>
//

//! Game `Board` Representation
//!
//! # Intro
//! _The game board implemented here corresponds to a solitaire game
//! developed by **Zachtronics** and which you can find in the `Shenzhen I/O` game.
//! This solitaire rules seems inspired by the `Freecell` solitaire game but with some
//! variations._
//!
//! This module provide the internal `Board` representation.
//! Each `Board` struct represents a state of a Game Board. \
//! A `Board` is represented by the differents areas where `Cards`
//! can be placed (Spaces, Goals, Piles, ...) and the `Cards` actually there.
//! A `Board` also contains a `Vec<Move>` which keeps track of all the `Move` which
//! has been necessary to obtain this Board from the initial one (auto moves not included).
//!
//! - The **spaces** corresponds to the 3 areas on the top left of the board, where
//!   only 1 `Card` can be placed temporarily and where all dragons should be "cleared" at the end.
//!
//! - The **goals** corresponds to the 3 areas on the top right of the board, where
//!   the numbered `Cards` should be placed in order to win the game.
//!
//! - The **piles** are the main areas in the center of the board where you can move `Cards`
//!   around.
//!
//! # Functionalities
//! The `Board` provides different methods in order to play with a given game state.
//!
//! You can get a `Vec` of all possible `Boards` you can obtain by 1 `Move` from the current `Board`.
//!
//! You can apply a given `Move` to the `Board`, which will alterate the current instance.
//!
//! The structure automatically apply the game 'automoves' which are movements made directly
//! by the game without user interaction under certain circumstances. \
//! These automove are performed when creating a new `Board` or after applying a `Move`.
//!

use std::fmt::{Display, Formatter};

use super::card::*;
use super::movement::*;

/// Number of Goals
pub const N_GOALS: usize = 3;
/// Number of Spaces
pub const N_SPACES: usize = N_GOALS;
/// Number of Piles
pub const N_PILES: usize = 8;

/// Internal representation of a game board
#[allow(clippy::derived_hash_with_manual_eq)]
#[derive(Clone, Hash)]
pub struct Board {
    spaces: [Space; N_SPACES],
    /// Indicates if flower is cleared.
    flower: bool,
    goals: [Option<Card>; N_GOALS],
    piles: [Vec<Card>; N_PILES],

    movements: Vec<Move>,
}

/// Represents possible cases for spaces.
/// Whether or not some `Card` is one the `Space`
/// or if Dragons have been locked in it.
///
/// It also provides which `Cards` are concerned if any
#[derive(Clone, Copy, PartialEq, Hash)]
pub enum Space {
    /// A card is on the space.
    Card(Card),
    /// Three dragons locked on the space.
    Dragon(Card),
    /// No card on the space.
    None,
}

impl Board {
    /// Creates a new board with given piles
    /// represented as an array a `Vec<Card>`.
    ///
    /// The created `Board` has empty `Spaces`, `Goals` and `Flower`
    /// (it is assumed that this is the beginning of the game).
    /// However, if some 'automoves' are available, they will be applied.
    pub fn new(piles: [Vec<Card>; N_PILES]) -> Self {
        let spaces = [Space::None; N_SPACES];
        let goals = [None; N_GOALS];
        let flower = false;
        let movements = Vec::new();

        let mut board = Board {
                spaces, flower, goals, piles,

                movements
        };

        // Apply eventual auto moves at start
        while let Some(auto_move) = board.auto_moves() {
            board._apply_move(auto_move);
        }

        board
    }

    /// Give access to the given pile.
    pub fn pile(&self, p_idx: usize) -> &Vec<Card> {
        &self.piles[p_idx]
    }

    /// Give access to the given space.
    pub fn space(&self, s_idx: usize) -> &Space {
        &self.spaces[s_idx]
    }

    /// Apply given `Move` to the `Board`
    /// and add the `Move` to the movement `Vec`.
    ///
    /// Also applies every possible auto (game made) movements
    /// (auto moves are not registered in movements list).
    ///
    /// Returns the number of automoves done.
    pub fn apply_move(&mut self, move_: Move) -> u16 {
        // Apply and register the move
        self._apply_move(move_);
        self.movements.push(move_);

        let mut n_auto: u16 = 0;
        // Apply all auto moves without registering
        while let Some(auto_move) = self.auto_moves() {
            self._apply_move(auto_move);
            n_auto += 1;
        }

        n_auto
    }

    /// Internal function used to apply moves.
    fn _apply_move(&mut self, move_: Move) {
        // Apply the move
        match move_ {
            Move::ToSpace{from_pile: p, to_space: s} => {
                self.spaces[s] = Space::Card(
                        self.piles[p].pop().unwrap()
                    );
            },
            Move::ToPile{from_space: s, to_pile: p} => {
                if let Space::Card(card) = self.spaces[s] {
                    self.piles[p].push(card);
                    self.spaces[s] = Space::None;
                }
            },
            Move::ToGoal{from, to_goal: g} => {
                match from {
                    Place::Pile(p) => {     // Pile -> Goal
                        self.goals[g] = Some(
                            self.piles[p].pop().unwrap()
                        );
                    },
                    Place::Space(s) => {    // Space -> Goal
                        if let Space::Card(card) = self.spaces[s] {
                            self.goals[g] = Some(card);
                            self.spaces[s] = Space::None;
                        }
                    }
                }
            },
            Move::PileToPile{n_cards, from, to} => {
                let mut cards = Vec::with_capacity(n_cards);
                // Get all cards from source pile
                for _ in 0..n_cards {
                    cards.push(self.piles[from].pop()
                            .expect("Not enough cards on pile")
                        );
                }
                // Put back cards in dest pile
                for card in cards.iter().rev() {
                    self.piles[to].push(*card);
                }
            },
            Move::ClearDragon{from, to_space: s} => {
                // Retrieve the card (with the first item in the array)
                let card = match from[0] {
                    Place::Pile(p) => *self.piles[p].last().expect("No Card on the pile"),
                    Place::Space(sp) => {
                        if let Space::Card(card) = self.spaces[sp] {
                            card
                        } else {
                            panic!("No Card in this space");
                        }
                    }
                };

                // Clears places where dragon are
                for place in from.iter() {
                    match place {
                        Place::Pile(p) => { self.piles[*p].pop().expect("No Dragon on this pile"); },
                        Place::Space(sp) => { self.spaces[*sp] = Space::None; },
                    }
                }

                // Put dragons into space
                self.spaces[s] = Space::Dragon(card);
            },
            Move::ClearFlower {from_pile: p} => {
                self.flower = true;
                self.piles[p].pop();
            }
        }
    }

    /// Returns a `Vec` containing all possible `Boards`
    /// which can be reached with one user `Move`
    /// (auto moves doesn't count).
    pub fn legal_moves(&self) -> Vec<Move> {
        let mut moves = Vec::new();

        moves.append(&mut self.move_to_goals());
        moves.append(&mut self.move_to_spaces());
        moves.append(&mut self.move_from_spaces());
        moves.append(&mut self.move_dragons());
        moves.append(&mut self.move_piles());

        moves
    }

    /// Returns the indices of free `Spaces`.
    fn free_spaces(&self) -> Vec<usize> {
        self.spaces.iter()
                .enumerate()                         // Get indices
                .filter(|(_, s)| s == &&Space::None) // Get empty spaces
                .map(|(i, _)| i )                    // Keep indices only
                .collect()
    }

    /// Returns the indices of free `Goals`.
    fn free_goals(&self) -> Vec<usize> {
        self.goals.iter()
                .enumerate()                  // Get indices
                .filter(|(_, s)| s.is_none()) // Get empty goals
                .map(|(i, _)| i )             // Keep indices only
                .collect()
    }

    /// Generate all the `Moves` **to** the **spaces**, if any legal.
    fn move_to_spaces(&self) -> Vec<Move> {
        let mut moves = Vec::new();

        // For all free spaces
        for space_idx in self.free_spaces() {
            // Go through all piles
            for (i, p) in self.piles.iter().enumerate() {
                if p.is_empty() { // Ignore empty piles
                    continue;
                }

                // Put the top card of the pile on the Space
                moves.push(
                        Move::ToSpace {
                            from_pile: i,
                            to_space: space_idx
                        }
                    );
            }
        }

        moves
    }

    /// Generate all the `Moves` **from** the **spaces**, if any legal.
    fn move_from_spaces(&self) -> Vec<Move> {
        let mut moves = Vec::new();

        // For all spaces
        for (sp_idx, space) in self.spaces.iter().enumerate() {
            if let Space::Card(card) = space { // If Card on it
                // Go through all piles
                for (p_idx, pile) in self.piles.iter().enumerate() {
                    // If pile is empty or the Card can go on top of it
                    if pile.is_empty() || card.goes_on(pile.last().unwrap()) {
                        moves.push( // Add this move
                                Move::ToPile {
                                    from_space: sp_idx,
                                    to_pile: p_idx
                                }
                            );
                    }
                }
            }
        }
        moves
    }

    /// Generate all `Moves` to the **goal** places, if any legal.
    fn move_to_goals(&self) -> Vec<Move> {
        let mut moves = Vec::new();

        // From piles to goals
        for (p_idx, pile) in self.piles.iter().enumerate() {
            if let Some(card) = pile.last() {                     // Get last card of the pile
                if let CardValue::Number(card_num) = card.value { // Only if card is number
                    let goal_idx = self.goal_stack(card.color);
                    let goal = &self.goals[goal_idx]; // Associated goal stack

                    // Check if the card can be put on the goal stack
                    if (goal.is_some() && goal.unwrap().value == CardValue::Number(card_num - 1)) ||
                            (goal.is_none() && card_num == 1) {

                        moves.push(
                                Move::ToGoal {
                                    from: Place::Pile(p_idx),
                                    to_goal: goal_idx
                                }
                            );
                    }
                }
            }
        }

        // From spaces to goals
        for (s_idx, space) in self.spaces.iter().enumerate() {
            if let Space::Card(card) = space {                    // Get card on space
                if let CardValue::Number(card_num) = card.value { // Only if number
                    let goal_idx = self.goal_stack(card.color);
                    let goal = &self.goals[goal_idx]; // Associated goal stack

                    // Check if card can go on goal stack
                    if (goal.is_some() && goal.unwrap().value == CardValue::Number(card_num - 1)) ||
                            (goal.is_none() && card_num == 1) {

                        moves.push(
                                Move::ToGoal {
                                    from: Place::Space(s_idx),
                                    to_goal: goal_idx
                                }
                            );
                    }
                }
            }
        }

    moves
    }

    /// Generate all `Move` for **clearing dragons**, if allowed.
    fn move_dragons(&self) -> Vec<Move> {
        let mut moves = Vec::new();

        // Compute Visible cards either on paces or on top of piles
        // with their associated position
        //  First, spaces
        let mut cards_in_front: Vec<(Card, Place)> =
                self.spaces.iter()
                        .enumerate()
                        .filter_map(|(idx, sp)| match sp {
                            Space::Card(card) => Some( // Get Cards only
                                    (card.to_owned(), Place::Space(idx))
                                ),
                            _ => None
                        })
                        .collect();

        //  Second, append piles
        cards_in_front.append(&mut
                self.piles.iter()
                        .enumerate()
                        .filter_map(|(idx, p)| // Get last Card on pile, if any
                            p.last().map(|card| (card.to_owned(), Place::Pile(idx)))
                        )
                        .collect()
            );

        // Get dragons from visible cards for each colors
        // (a bit heavy, ...)
        let red_dragons: Vec<(Card, Place)> = cards_in_front.iter()
                .filter(|(c, _)| *c == Card::dragon(CardColor::Red))
                .copied()
                .collect();
        let green_dragons: Vec<(Card, Place)> = cards_in_front.iter()
                .filter(|(c, _)| *c == Card::dragon(CardColor::Green))
                .copied()
                .collect();
        let black_dragons: Vec<(Card, Place)> = cards_in_front.iter()
                .filter(|(c, _)| *c == Card::dragon(CardColor::Black))
                .copied()
                .collect();

        // For each list of accessible dragons
        for dragons in [red_dragons, green_dragons, black_dragons].iter() {
            // If enough dragons
            if dragons.len() == 4 {
                // Get possible destinations (either on empty space or on space where a dragon is)
                let available_spaces: Vec<usize> = self.spaces.iter()
                        .enumerate()
                        .filter(|(_, s)| **s == Space::None || **s == Space::Card(dragons[0].0))
                        .map(|(s_idx, _)| s_idx)
                        .collect();

                if available_spaces.is_empty() {
                    continue;
                }

                // Use the first one
                // Maybe this should be changed to use dragon
                // in space (if any) in priority
                let to_space = available_spaces[0];

                // Collect dragon places into a Vector first
                let fvec: Vec<Place> = dragons.iter()
                        .map(|(_, p)| p)
                        .copied()
                        .collect();
                // And fill an arry with it (very ugly code, though)
                let from = [fvec[0], fvec[1], fvec[2], fvec[3]];

                moves.push(
                        Move::ClearDragon {from, to_space}
                    );
            }
        }
        moves
    }

    /// Generate all `Moves` between **piles**.
    fn move_piles(&self) -> Vec<Move> {
        let mut moves = Vec::new();
        for (p_idx, pile) in self.piles.iter().enumerate() {
            if let Some(top) = pile.last() {
                // Travel up if cards can move together
                let mut prev = top;
                for (n_cards, next) in pile.iter().rev().enumerate() {
                    if n_cards > 0 && !prev.goes_on(next) {
                        break; // Next card no longer part of the group
                    }
                    prev = next;

                    // Check if it/they can go on another pile
                    for (dst_idx, dst_pile) in self.piles.iter().enumerate() {
                        if dst_idx == p_idx { // dest == source
                            continue;
                        }

                        if dst_pile.is_empty() || next.goes_on(dst_pile.last().unwrap()) {
                            moves.push(
                                    Move::PileToPile {
                                        n_cards: n_cards + 1,
                                        from: p_idx,
                                        to: dst_idx
                                    }
                                );
                        }
                    }
                }
            }
        }

        moves
    }

    /// If an automatic (game-made) `Move` is available, return it; \
    /// return `None` otherwise.
    ///
    /// `Cards` automove into their stack if:
    /// - Card number is 1
    /// - Card number is less than or equal to (the minimum number on goals + 1)
    fn auto_moves(&self) -> Option<Move> {
        let mut max_goal_val: u8 = 10; // The maximum Card value that will be auto moved on goal
        // This max value is equal to the minimum card value on all goals + 1
        for g in &self.goals {
            max_goal_val = match g {
                None => 2,
                Some(card) => {
                    if let CardValue::Number(card_num) = card.value {
                        let val = std::cmp::max(2, card_num + 1); // 1 and 2 can always be put on
                        std::cmp::min(max_goal_val, val)
                    } else {
                        panic!("Impossible Card on goal: {}", card);
                    }
                }
            };
        }

        // Piles to goals
        for (p_idx, pile) in self.piles.iter().enumerate() {
            if let Some(card) = pile.last() {                     // Get last card of the pile
                if let CardValue::Number(card_num) = card.value { // Only if card is number
                    let goal_idx = self.goal_stack(card.color);
                    let goal = &self.goals[goal_idx]; // Associated goal stack

                    // Check if the card can be put on the goal stack
                    if ((goal.is_some() && goal.unwrap().value == CardValue::Number(card_num - 1)) ||
                            (goal.is_none() && card_num == 1)) &&
                            card_num <= max_goal_val {
                        return Some(
                            Move::ToGoal {
                                from: Place::Pile(p_idx),
                                to_goal: goal_idx
                            }
                        );
                    }
                // Clear flower, if available
                } else if card.value == CardValue::Flower {
                    return Some(
                            Move::ClearFlower { from_pile: p_idx }
                        );
                }
            }
        }

        // spaces to goals
        for (s_idx, space) in self.spaces.iter().enumerate() {
            if let Space::Card(card) = space {                    // Get card on space
                if let CardValue::Number(card_num) = card.value { // Only if number
                    let goal_idx = self.goal_stack(card.color);
                    let goal = &self.goals[goal_idx];

                    // Check if card can go on goal stack
                    if ((goal.is_some() && goal.unwrap().value == CardValue::Number(card_num - 1)) ||
                            (goal.is_none() && card_num == 1)) &&
                            card_num <= max_goal_val {
                        return Some(
                            Move::ToGoal {
                                from: Place::Space(s_idx),
                                to_goal: goal_idx
                            }
                        );
                    }
                }
            }
        }
        None
    }

    /// Gives the goal stack index corresponding to the given `CardColor`.
    ///
    /// Their is no predefined rule for the color of a stack.
    /// The first stack available is taken by the first `Card` which can
    /// be moved to a goal.
    ///
    /// This function first look if a `Card` with the same color is already
    /// on one of the `Goal` and returns the index in this case. Otherwise,
    /// the function just return the first available `Goal` index.
    fn goal_stack(&self, color: CardColor) -> usize {
        // If a Card of this color is already on a goal, return it
        for (i, g) in self.goals.iter().enumerate() {
            if let Some(card) = g {
                if card.color == color {
                    return i;
                }
            }
        }

        // Or return the first available goal
        self.free_goals()[0]
    }

    /// Gives the index of the **circle** (ordered from top to bottom) corresponding
    /// to given `CardColor`
    ///
    /// As opposed to the `Goals` each color has its own circle.
    /// - The top one is for the **Red** Dragons.
    /// - The 2nd one is for the **Green** ones.
    /// - The last one is for the **Black** ones.
    ///
    pub fn circle_index(color: CardColor) -> usize {
        match color {
            CardColor::Red    => 0,
            CardColor::Green  => 1,
            CardColor::Black  => 2,
            c => panic!("No circle corresponding to color {}", c),
        }
    }

    /// Returns the `Vec` containing all user `Move` applied to this `Board`.
    ///
    /// _("user" `Move` as opposed to "automoves" which are not included)._
    pub fn movements(&self) -> &Vec<Move> {
        &self.movements
    }

    /// Check if the `Board` is cleared.
    pub fn is_cleared(&self) -> bool {
        // Flower should be cleared
        if !self.flower {
            return false;
        }

        // All piles must be empty
        for p in self.piles.iter() {
            if !p.is_empty() {
                return false;
            }
        }

        // All goals should be number 9
        for g in self.goals.iter() {
            if let Some(card) = g {
                if let CardValue::Number(9) = card.value {}
                else {
                    return false;
                }
            } else {
                return false;
            }
        }

        // All spaces must be locked
        for s in self.spaces.iter() {
            match s {
                Space::Dragon(_) => (),
                _ => return false
            };
        }

        true
    }

    /// Distance from solution heuristic.
    ///
    /// This function compute a heuristic score depending on how
    /// far this `Board` is from the cleared state.
    ///
    /// The lower the score is, the closest the `Board` is from the resolution.
    pub fn score(&self) -> u32 {
        // Pile len
        let mut score: u32 = self.piles.iter()
                .map(|p| p.len() as u32)
                .sum();

        // Spaces
        for s in self.spaces.iter() {
            score += match s {
                Space::Card(card) => {
                    match card.value {
                        CardValue::Number(_) => 10,
                        CardValue::Dragon => 1,
                        CardValue::Flower => 1000
                    }
                }
                _ => 0
            }
        }

        // Piles
        // depend on the card and if card are grouped together
        for pile in self.piles.iter() {
            let mut prev: Option<Card> = None;
            for card in pile.iter() {
                if let Some(prev_card) = prev {
                    if !prev_card.goes_on(card) {
                        score += 10;
                    }
                }
                prev = Some(*card);
            }
            if prev.is_some() {
                score += 10
            }
        }
        score
    }
}

impl Display for Board {
    /// Display the board graphically
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        // Display Spaces
        write!(f, "|")?;
        for s in self.spaces.iter() {
            match s {
                Space::Card(card) => write!(f, " {}", card)?,
                Space::Dragon(_)  => write!(f, " XX")?,
                Space::None       => write!(f, " __")?,
            }
        }

        // Flower
        write!(f, " | |")?;
        if self.flower {
            write!(f, "{}", Card::flower())?;
        } else {
            write!(f, "__")?;
        }

        // Goals
        write!(f, "| |")?;
        for g in self.goals.iter() {
            if let Some(card) = g {
                write!(f, " {}", card)?;
            } else {
                write!(f, " __")?;
            }
        }
        writeln!(f, " |")?;

        // Board
        let mut still_cards = true;
        let mut idx = 0;
        while still_cards {
            write!(f, "\n   ")?;
            still_cards = false;
            for pile in self.piles.iter() {
                if let Some(card) = pile.get(idx) {
                    still_cards = true;
                    write!(f, " {}", card)?;
                } else {
                    write!(f, "   ")?;
                }
            }
            idx += 1;
        }
        Ok(())
    }
}

impl PartialEq for Board {
    fn eq(&self, other: &Self) -> bool {
        self.spaces == other.spaces &&
        self.goals == other.goals &&
        self.flower == other.flower &&
        self.piles == other.piles
    }
}

impl Eq for Board {}

#[cfg(test)]
mod tests {
    use super::*;

    /// Test the Board::is_clear method
    #[test]
    fn test_clear() {
        // Cleared spaces
        let spaces = [
                Space::Dragon(Card::dragon(CardColor::Red)),
                Space::Dragon(Card::dragon(CardColor::Green)),
                Space::Dragon(Card::dragon(CardColor::Black))
            ];
        // Cleared flower
        let flower = true;
        // Card number 9 in each goals
        let goals: [Option<Card>; N_GOALS] = [
            Some(Card::number(CardColor::Red, 9)),
            Some(Card::number(CardColor::Green, 9)),
            Some(Card::number(CardColor::Black, 9)),
        ];
        // Empty piles
        let piles: [Vec<Card>; N_PILES] = Default::default();

        // Whatever
        let movements = Vec::new();

        let board = Board {
            spaces,
            flower,
            goals,
            piles,
            movements
        };

        assert!(board.is_cleared());
        // A cleared Board should have a score of 0
        assert_eq!(board.score(), 0);
    }
}
