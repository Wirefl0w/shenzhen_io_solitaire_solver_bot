//
// Copyright (C) 2021, 2023-2024  Florent "Wirefl0w" C.
//
// This file is part of shenzhen_io_solitaire_solver_bot
//
// shenzhen_io_solitaire_solver_bot is free software:
// you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>
//

//! Card internal representation
//!
//! This module provide the internal representation of game `Card`.
//!
//! A [`Card`] consists in a struct represented by:
//! - Its [color](CardColor)
//! - Its [value](CardValue)
//!
//! Both of them are represented by enums.
//!
//! There are 3 different colors in this game:
//! - Red
//! - Green
//! - Black
//!
//! `Card` values are either numbers from `1` to `9` or a special value called `Dragon`.
//! Each color have 1 `Card` for each number and 4 `Dragons`.
//!
//! There is also a special `Card`: the `Flower` card.

use std::fmt::{Display, Formatter};

/// Internal Representation of a Card
///
/// A Card is represented as a struct with 2 fields
/// corresponding to its color and value.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct Card {
    pub color: CardColor,
    pub value: CardValue,
}

/// Representation of the [Card] color.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum CardColor {
    Flower,
    Red,
    Green,
    Black,
}

/// Representation of the [Card] value.
#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub enum CardValue {
    Flower,
    Dragon,
    Number(u8),
}

impl Card {
    /// Create a new [Card] with the given [CardColor] and [CardValue].
    pub fn new(color: CardColor, value: CardValue) -> Self {
        Card { color, value }
    }

    /// Returns a `Vec` containing 1 instance of every existing [Card].
    pub fn all() -> Vec<Self> {
        let mut cards = Vec::new();

        // Create Cards for all colors
        for color in &[CardColor::Red, CardColor::Green, CardColor::Black] {
            // Create each number Cards
            for value in (1..10).map(CardValue::Number).collect::<Vec<CardValue>>() {
                cards.push(Card::new(*color, value));
            }
            // Add the Dragon
            cards.push(Card::dragon(*color));
        }
        // Add the flower Card
        cards.push(Card::flower());

        cards
    }

    /// Returns the `Flower Card`.
    pub fn flower() -> Self {
        Card {
            color: CardColor::Flower,
            value: CardValue::Flower,
        }
    }

    /// Returns a `Dragon Card` with the given [CardColor].
    pub fn dragon(color: CardColor) -> Self {
        Card {
            color,
            value: CardValue::Dragon,
        }
    }

    /// Returns a `Number Card` with the given number and [CardColor].
    pub fn number(color: CardColor, num: u8) -> Self {
        Card {
            color,
            value: CardValue::Number(num),
        }
    }

    /// Check whether this Card can goes on top of other in a pile.
    ///
    /// In order for a [Card] to go on top of another in the game piles,
    /// the card should have a different `color` and its number should be 1 time lower
    /// than the previous [Card].
    ///
    /// `Dragons` cannot be put on top of any [Card] and no [Card] can be put on them.
    pub fn goes_on(&self, other: &Card) -> bool {
        // Dragon of flower => false
        if let (CardValue::Number(num1), CardValue::Number(num2))
                = (self.value, other.value) {
            self.color != other.color && num1 + 1 == num2
        } else {
            false
        }
    }
}

impl Display for Card {
    /// Representation of the `Card` with **two characters** specifying
    /// the `Color` and the `Value` (1 character each)
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}{}", self.color, self.value)
    }
}

impl Display for CardColor {
    /// Represents the `Color` of a `Card` with **1 character**.
    ///
    /// _Usually the first letter of the color or `F` for the `Flower Card`.
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            CardColor::Flower => write!(f, "F"),
            CardColor::Red    => write!(f, "R"),
            CardColor::Green  => write!(f, "G"),
            CardColor::Black  => write!(f, "B"),
        }
    }
}

impl Display for CardValue {
    /// Represents the `Value` of a `Card` with **1 character**.
    ///
    /// The character can be a number,
    /// `D` for a `Dragon Card` and `F` for the `Flower Card`.
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            CardValue::Flower => write!(f, "F"),
            CardValue::Dragon => write!(f, "D"),
            CardValue::Number(num) => write!(f, "{}", num),
        }
    }
}
