//
// Copyright (C) 2021, 2023-2024  Florent "Wirefl0w" C.
//
// This file is part of shenzhen_io_solitaire_solver_bot
//
// shenzhen_io_solitaire_solver_bot is free software:
// you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>
//

//! Internal representation of a game Movement
//!
//! [Movements](Move) are represented with enums corresponding to
//! a movement of [Card](super::Card) through different places on the [Board](super::Board).
//!
//! [Movements](Move) are used to update a [Board](super::Board) by changing
//! the place of its [Cards](super::Card).

/// Enumeration representing a `Movements` applicable to a game `Board`.
#[derive(Clone, Copy, PartialEq, Hash, Debug)]
pub enum Move {
    ToSpace { from_pile: usize, to_space: usize },
    ToPile  { from_space: usize, to_pile: usize },
    ToGoal  { from: Place, to_goal: usize},
    /// Movements from a pile to another can be done
    /// with more than one card (`n_card` paramter)
    PileToPile { n_cards: usize, from: usize, to: usize},
    /// When clearing Dragons, 4 Cards are needed
    /// they can be either on a space or on a pile
    ClearDragon {from: [Place; 4] , to_space: usize},
    ClearFlower {from_pile: usize},
}

/// Enum representing either a `Pile` or a `Space`
/// with their corresponding index
#[derive(Clone, Copy, PartialEq, Hash, Debug)]
pub enum Place {
    Pile(usize),
    Space(usize)
}
