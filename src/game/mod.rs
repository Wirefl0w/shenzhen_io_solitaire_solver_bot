//
// Copyright (C) 2021, 2023-2024  Florent "Wirefl0w" C.
//
// This file is part of shenzhen_io_solitaire_solver_bot
//
// shenzhen_io_solitaire_solver_bot is free software:
// you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>
//

//! Module implementing the internal game logic.
//!
//! # Intro
//!
//! This Module implements the game logic of the solitaire card game
//! in the game "Shenzhen I/O".
//!
//! It contains internal representation
//! of [Card]s, [Board], [Move]ments, etc... (And also a game solver)
//!
//! # Game Solver
//!
//! The solver works by exploring all possible movements from the initial
//! [Board]. It makes use of a priority queue associated with a heuristic Board score
//! to proiritize Boards that are closer to the solution
//!

use std::time::{Instant, Duration};
use std::cmp::Reverse;

use priority_queue::PriorityQueue;

mod card;
mod board;
mod movement;

pub use card::*;
pub use board::*;
pub use movement::*;

/// Minimal delay between two solver info prints.
const PRINT_TIMEOUT: Duration = Duration::from_millis(150);

/// Given a game `Board`, returns the list of `Move` to clear the board.
/// If time out or no solution is found, returns `None`.
pub fn find_solution(init_board: Board, timeout: Option<Duration>) -> Option<Vec<Move>> {
    // time measurement
    let time = Instant::now(); // From the beginning
    let mut print_time = time; // used to limit information printing

    let mut seen: Vec<Board> = vec!(init_board.clone()); // The list of already seen Boards
    let mut queue = PriorityQueue::<Board, Reverse<u32>>::new();  // The queue of Boards to explore
    let score = init_board.score();
    queue.push(init_board, Reverse(score));

    let mut n_states = 0; // Number of states explored
    let mut finished: Option<Board> = None; // The Solution Board

    // Print state before first iteration
    print!("\n\n\n");
    print_info(n_states, queue.len(), time.elapsed().as_secs_f32());

    while !queue.is_empty() && finished.is_none() {
        let curr_board = queue.pop().unwrap().0;

        // Checking timeout
        if let Some(timeout) = timeout {
            if time.elapsed() > timeout {
                println!("Timed out! No solution found.");
                return None;
            }
        }

        // Printing solver information
        if print_time.elapsed() > PRINT_TIMEOUT {
            print_info(n_states, queue.len(), time.elapsed().as_secs_f32());
            print_time = Instant::now();
        }

        n_states += 1;

        // Go through all possible moves of the current Board
        for move_ in curr_board.legal_moves() {
            // Create a new Board with the move applied
            let mut board = curr_board.clone();
            board.apply_move(move_);

            if !seen.contains(&board) { // If the board hasn't been seen yet
                if board.is_cleared() { // Check if its is clear
                    finished = Some(board);
                    break;
                }
                // If not clear, add it to seen Boards and push it to queue
                seen.push(board.clone());
                let score = board.score();
                queue.push(board, Reverse(score));
            }
        }
    }

    // Solver Done, print info one last time
    print_info(n_states, queue.len(), time.elapsed().as_secs_f32());

    if let Some(solution) = finished {
        println!("Solution found.");

        Some(solution.movements().to_owned())
    } else {
        println!("No Solution found.");

        None
    }
}

/// Function to display informations about
/// the working solver.
///
/// It displays 3 lines containing provided informations.
/// This function erase the 3 last lines before printing,
/// so you should print 3 empty lines before calling
/// it first. Then, by calling it repeatedely, the information
/// printed look like updating itself.
fn print_info(n_states: u32, queue_len: usize, time: f32) {
    print!("\x1b[3A");  // Go 3 lines up
    println!("States explored:   {}", n_states);
    println!("Elements in queue: {}", queue_len);
    println!("Elapsed time:      {}s        ", time);
}
