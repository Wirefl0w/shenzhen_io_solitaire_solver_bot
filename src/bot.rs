//
// Copyright (C) 2021, 2023-2024  Florent "Wirefl0w" C.
//
// This file is part of shenzhen_io_solitaire_solver_bot
//
// shenzhen_io_solitaire_solver_bot is free software:
// you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>
//

//! Module implementing all Bot functionalities
//!
//! This module provide the `GameBot` Struct which is able
//! to emulate mouse events and play the game.
//!
//! The structure needs to be initialized with an instance
//! of a `BoardImage` in order to know at which coordinates
//! to moves depending on the considered `Move`ment.
//!
//! Before starting solving a game, the `GameBot` also needs
//! an instance of the actual `Board` it will solve, in order
//! to keep track of the number of Cards on each pile.

use std::thread::sleep;
use std::time::Duration;
use std::error::Error;

use crate::game::{Board, Space, Move, Place};
use crate::boardimage::BoardImage;

use mouse_rs::{types::keys::Keys, Mouse};

/// A gneric result type used by the `GameBot` methods.
type Result = core::result::Result<(), Box<dyn Error>>;

/// Bot Object eumlating mouse events
/// in order to play some `Move`ments in the game.
pub struct GameBot {
    board: Option<Board>,
    img: Option<BoardImage>,

    pos: (i32, i32),
    mouse: Mouse,
}

/// All possible places to move a Card to and from. \
/// Indices goes from left to right.
enum Location {
    /// The first `usize` gives the **pile index**, the 2nd gives the **card index**.
    Card(usize, usize),
    Space(usize),
    /// Circles are order from top to bottom _(red, green, black)_
    Circle(usize),
    Goal(usize),
}

/// Number of steps made by the mouse during a smooth move.
const MOUSE_STEPS: i32 = 150;

/// The delay in between each mouse movement steps.
const DELAY_MOUSE_STEP: u64 = 1;
/// Delay for reliability in between mouse button state change.
const DELAY_RELIABILITY: u64 = 20;
/// Delay of button press for a single click.
const DELAY_SINGLE_CLICK: u64 = 50;
/// Delay to wait for each auto move.
const DELAY_AUTO_MOVE: u64 = 250;
/// Delay in between each move.
const DELAY_MOVE: u64 = 200;

impl GameBot {
    /// Create a new Bot object capable of emulating mouse events.
    /// The bot should be initialized with two other methods:
    ///  - `GameBot.init()`  with a BoardImage instance (need to be called once).
    ///  - `GameBot.start()` with a Board instance identified from the BoardImage
    ///                     (need to be called before starting to solve a game).
    ///
    /// Both functions are separated to allow clicking on new game even
    /// if `Board` cannot be identified.
    pub fn new() -> Self {
        let mouse = Mouse::new();
        let img = None;
        let board = None;
        let pos = (0, 0); // mouse.get_position() is not implemented on linux
                          // So wee keep track of it in this variable

        GameBot {
            board,
            img,
            pos,
            mouse,
        }
    }

    /// Initialize the `GameBot` with a `BoardImage`. \
    /// The `BoardImage` allow the Bot to know at which coordinates
    /// to click to perform a given `Move` \
    /// This function can be called only once if screen resolution doesn't
    /// change during runtime.
    pub fn init(&mut self, img: BoardImage) {
        self.img = Some(img);
    }

    /// Next initialization step with the initial Board state.
    ///
    /// The Board state is needed so the Bot knows how many Cards are
    /// on pile to know where to click to displace Cards.
    pub fn start(&mut self, board: Board) {
        self.board = Some(board);
    }

    /// Performs a smooth mouse movement from current position to given position.
    /// The mouse will make 'MOUSE_STEPS' steps during the movement.
    fn move_to_coords(&mut self, x: i32, y: i32) -> Result {
        // Compute source position
        //let pos = self.mouse.get_position()?; // unimplemented on Linux
        //let (src_x, src_y) = (pos.x as i32, pos.y as i32);

        let (src_x, src_y) = self.pos;
        // Compute the step for each direction
        let step_x = (x - src_x) as f32 / MOUSE_STEPS as f32;
        let step_y = (y - src_y) as f32 / MOUSE_STEPS as f32;

        for i in 1..=MOUSE_STEPS {
            // Move one step forward
            let sx = src_x + (step_x * i as f32) as i32;
            let sy = src_y + (step_y * i as f32) as i32;
            self.mouse.move_to(sx, sy)?;
            sleep(Duration::from_millis(DELAY_MOUSE_STEP));
        }
        self.pos = (x, y);
        Ok(())
    }

    /// Move the mouse to the position of its arguments
    /// represented by a `Location` enum variant.
    fn move_to(&mut self, place: Location) -> Result {
        let img = self.img.as_ref()
                .expect("GameBot.init() should be called before.");
        let (x, y) = match place {
            Location::Card(p_idx, c_idx) => img.pile_click_coords(p_idx as u8, c_idx as u8),
            Location::Space(s_idx)  => img.space_coords(s_idx as u8),
            Location::Goal(g_idx)   => img.goal_coords(g_idx as u8),
            Location::Circle(c_idx) => img.circle_coords(c_idx as u8)
        };
        self.move_to_coords(x as i32, y as i32)?;
        Ok(())
    }

    /// Click and hold the left button of the mouse.
    fn click(&self) -> Result {
        sleep(Duration::from_millis(DELAY_RELIABILITY));
        self.mouse.press(&Keys::LEFT)?;
        sleep(Duration::from_millis(DELAY_RELIABILITY));
        Ok(())
    }

    /// Release the left button of the mouse.
    fn release(&self) -> Result {
        self.mouse.release(&Keys::LEFT)?;
        sleep(Duration::from_millis(DELAY_RELIABILITY));
        Ok(())
    }

    /// Perform a single click (and release) input with mouse.
    fn click_once(&self) -> Result {
        self.click()?;
        sleep(Duration::from_millis(DELAY_SINGLE_CLICK));
        self.release()?;
        Ok(())
    }

    /// Click on the top left of the screen to catch game window focus.
    pub fn catch_focus(&mut self) -> Result {
        self.move_to_coords(100, 100)?;
        self.click_once()?;
        Ok(())
    }

    /// Click on the new game button.
    pub fn click_new_game(&mut self) -> Result {
        let (x, y) = self.img.as_ref()
                .expect("GameBot.init() should be called before.")
                .new_game_coords();
        self.move_to_coords(x as i32, y as i32)?;
        self.click_once()?;
        Ok(())
    }

    /// Make the Bot performs the given movement.
    pub fn play_move(&mut self, move_: Move) -> Result {
        let board = &self.board.as_ref()
                .expect("GameBot.start() should be called before.");

        match move_ {
            Move::ToSpace {from_pile, to_space} => {
                // Get top card index and move to it
                let c_idx = board.pile(from_pile).len();
                self.move_to(Location::Card(from_pile, c_idx))?;
                // Grab the card and move it to space
                self.click()?;
                self.move_to(Location::Space(to_space))?;
                self.release()?;
            },
            Move::ToPile {from_space, to_pile}  => {
                // Get destination top card index
                let c_idx = board.pile(to_pile).len();

                // Grab Card from space
                self.move_to(Location::Space(from_space))?;
                self.click()?;
                // Move to destination
                self.move_to(Location::Card(to_pile, c_idx))?;
                self.release()?;
            },
            Move::ToGoal {from, to_goal} => {
                match from {
                    Place::Pile(p_idx) => {
                        // Get top card index and move there
                        let c_idx = board.pile(p_idx).len();
                        self.move_to(Location::Card(p_idx, c_idx))?;
                    },
                    Place::Space(s_idx) => {
                        self.move_to(Location::Space(s_idx))?;
                    }
                }
                self.click()?;
                self.move_to(Location::Goal(to_goal))?;
                self.release()?;
            },
            Move::PileToPile {n_cards, from, to} => {
                // Get source and destination cards index
                let src_c_idx = board.pile(from).len() - n_cards;
                let dst_c_idx = board.pile(to).len();

                // Move to source
                self.move_to(Location::Card(from, src_c_idx))?;
                self.click()?;
                // Move to destination
                self.move_to(Location::Card(to, dst_c_idx))?;
                self.release()?;
            },
            Move::ClearDragon {from, ..} => {
                // Get card color
                let color = match from[0] {
                    Place::Pile(p_idx) => board.pile(p_idx).last()
                            .expect("No card found on given pile").color,
                    Place::Space(s_idx) => match board.space(s_idx) {
                        Space::Card(card) => card.color,
                        _ => panic!("No card found on given pile"),
                    },
                };
                // Get dragon circle index
                let idx = Board::circle_index(color);
                self.move_to(Location::Circle(idx))?;

                self.click_once()?;
            },
            Move::ClearFlower {..} => (),
        }

        // Update board object
        let n_auto = self.board.as_mut().unwrap().apply_move(move_);

        // Sleep for some time to let each automoves finish
        sleep(Duration::from_millis(DELAY_AUTO_MOVE * n_auto as u64));

        // Sleep for some time between each move
        sleep(Duration::from_millis(DELAY_MOVE));

        Ok(())
    }
}
