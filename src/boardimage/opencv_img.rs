//
// Copyright (C) 2021, 2023-2024  Florent "Wirefl0w" C.
//
// This file is part of shenzhen_io_solitaire_solver_bot
//
// shenzhen_io_solitaire_solver_bot is free software:
// you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>
//

//! Module providing image recognition
//! routines through the [`opencv`] crates.

use opencv::core::Range;
use opencv::core::{Rect, Point, VecN};
use opencv::core::{Vector, Mat, MatTraitConstManual};
use opencv::core::{min_max_loc, no_array};
use opencv::core::Size;
use opencv::imgcodecs::{imdecode, imread, imwrite, IMREAD_COLOR};
use opencv::imgproc::{match_template, TM_CCORR_NORMED};
use opencv::imgproc::{resize, INTER_LINEAR};
use opencv::imgproc::{rectangle, circle, LINE_8};

const RED: VecN<f64, 4> = VecN::new(0.0, 0.0, 255.0, 255.0);
const GREEN: VecN<f64, 4> = VecN::new(0.0, 255.0, 0.0, 255.0);

/// Compatibility with `image`/`imageproc`.
/// In this case, `OpenCvImg` uses color images so it can be drawned on.
pub(super) type OpenCvDrawImg = OpenCvImg;

/// Wrapper around [`opencv::core::Mat`] providing necessary manipulation &
/// detection functions for [`BoardImage`](super::BoardImage).
#[derive(Clone)]
pub(super) struct OpenCvImg {
    image: Mat,
}

impl OpenCvImg {
    /// Loads the image from image data.
    pub fn load(img: Vec<u8>) -> Self {
        let image = imdecode(
                &Vector::from_slice(img.as_slice()),
                IMREAD_COLOR).unwrap();

        OpenCvImg {
            image
        }
    }

    /// Opens the image from a file.
    pub fn open(filepath: &str) -> Self {
        let image = imread(filepath, IMREAD_COLOR).unwrap();

        OpenCvImg {
            image
        }
    }

    /// Returns a new image containing only the rect of the given
    /// dimensions and which top-left corner is at `(x, y)`
    pub fn sub_img(&self, x: u32, y: u32, width: u32, height: u32) -> Self {
        let x: i32 = x.try_into().unwrap();
        let y: i32 = y.try_into().unwrap();
        let width: i32 = width.try_into().unwrap();
        let height: i32 = height.try_into().unwrap();

        let image = Mat::rowscols(&self.image,
                                  &Range::new(y, y+height).unwrap(),
                                  &Range::new(x, x+width).unwrap()).unwrap();


        OpenCvImg {
            image,
        }
    }

    /// Returns the size `(width, height)` of the Image object in pixel.
    pub fn size(&self) -> (u32, u32) {
        let size = self.image.size().unwrap();

        (size.width.try_into().unwrap(), size.height.try_into().unwrap())
    }

    /// Resizes the image to the given dimensions.
    pub fn resize(&mut self, width: u32, height: u32) {
        let width: i32 = width.try_into().unwrap();
        let height: i32 = height.try_into().unwrap();

        let mut new_img = self.image.clone();
        resize(&self.image,
               &mut new_img,
               Size::new(width, height),
               0.0,
               0.0,
               INTER_LINEAR).unwrap();

        self.image = new_img;
    }

    /// Performs template matching against `other`
    /// and returns the best matching score.
    pub fn match_template(&self, other: &Self) -> f64 {
        let mut res = Mat::default();

        match_template(&self.image, &other.image, &mut res, TM_CCORR_NORMED, &no_array()).unwrap();

        let mut res_score = 0f64;
        min_max_loc(&res, None, Some(&mut res_score), None, None, &no_array()).unwrap();

        res_score
    }

    // Drawing

    /// Compatibility with `ImageProcImg`.
    pub fn from_img(img: &Self) -> Self {
        img.clone()
    }

    /// Saves the image at the given filepath.
    pub fn save(&self, filepath: &str) {
        imwrite(filepath, &self.image, &Vector::new()).unwrap();
    }

    /// Draw a red rectangle of the given size which top left corner is
    /// at given coordinates.
    pub fn draw_detection_zone(&mut self, x: u32, y: u32, width: u32, height: u32) {
        let x: i32 = x.try_into().unwrap();
        let y: i32 = y.try_into().unwrap();
        let width: i32 = width.try_into().unwrap();
        let height: i32 = height.try_into().unwrap();

        rectangle(&mut self.image,
                  Rect {x, y, width, height},
                  RED, 1, LINE_8, 0).unwrap();
    }

    /// Draw a filled green circle with the given `(x, y)` on the image.
    pub fn draw_click_zone(&mut self, x: u32, y: u32) {
        let x: i32 = x.try_into().unwrap();
        let y: i32 = y.try_into().unwrap();

        circle(&mut self.image,
               Point {x, y},
               5, GREEN, -1, LINE_8, 0).unwrap();
    }
}
