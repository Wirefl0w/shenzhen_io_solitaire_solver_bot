//
// Copyright (C) 2021, 2023-2024  Florent "Wirefl0w" C.
//
// This file is part of shenzhen_io_solitaire_solver_bot
//
// shenzhen_io_solitaire_solver_bot is free software:
// you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>
//

//! Constants defining different dimensions of the game Board.
//!
//! These dimensions are represented as a fraction of the image size.
//!
//! Horizontal distances are ratios of the image width.
//! Vertical distances are ratios of the image height.

/// Distance between the left of the screen and the left of the game
/// screen containing the Solitaire game.
pub(super) const GAME_SCREEN_LEFT_BORDER: f32 = 0.1495;
/// Distance between the top of the screen and the top of the game
/// screen containing the Solitaire game.
pub(super) const GAME_SCREEN_TOP_BORDER: f32 = 0.0981;

// Spaces
/// Distance between the left border of the image
/// and the center of the first space (Horizontal)
pub(super) const LEFT_TO_SPACE: f32 = 0.1300;
/// Distance between the center of two spaces (Horizontal)
pub(super) const SPACE_TO_SPACE: f32 = 0.1120;

/// Distance between the top of the image
/// and the center of the spaces (Vertical)
pub(super) const TOP_TO_SPACES: f32 = 0.1670;

// Goals
/// Distance between the left border of the image
/// and the center of the first goal (Horizontal)
pub(super) const LEFT_TO_GOAL: f32 = 0.7020;
/// Distance between the center of two goals (Horizontal)
pub(super) const GOAL_TO_GOAL: f32 = SPACE_TO_SPACE;

/// Distance between the top of the image
/// and the center of the goals (Vertical)
pub(super) const TOP_TO_GOALS: f32 = TOP_TO_SPACES;

// Flower
/// Distance between the top of the image
/// and the center of the flower goal (Vertical)
pub(super) const TOP_TO_FLOWER: f32 = TOP_TO_SPACES;
/// Distance between the left border of the image
/// and the center of the flower goal (Horizontal)
pub(super) const LEFT_TO_FLOWER: f32 = 0.5440;


// Dragons circles
/// Distance between the left border of the image
/// and the center of the dragon circles (Horizontal)
pub(super) const LEFT_TO_CIRCLES: f32 = 0.4480;

/// Distance between the top of the image
/// and the center of the 1st dragon circle (Vertical)
pub(super) const TOP_TO_CIRCLE: f32 = 0.0620;
/// Distance between the center of 2 dragon circles (Vertical)
pub(super) const CIRCLE_TO_CIRCLE: f32 = 0.0950;

// Piles
/// Distance between the left border of the image
/// and the center of the first pile (Horizontal)
pub(super) const LEFT_TO_PILE: f32 = 0.1380;
/// Distance between the center of two piles (Horizontal)
pub(super) const PILE_TO_PILE: f32 = 0.1130;

// Cards
/// Width of a card (Horizontal)
pub(super) const CARD_WIDTH: f32 = 0.1000;
/// Height of a card (Vertical)
pub(super) const CARD_HEIGHT: f32 = 0.2800;

/// Distance between the top of the image
/// and the center of the first card on the pile (Vertical)
pub(super) const TOP_TO_PILES: f32 = 0.4690;
/// Distance between the center of 2 following cards (Vertical)
pub(super) const CARD_OFFSET: f32 = 0.0370;

/// Width of the card corner for card identification (Horizontal)
pub(super) const TEMPLATE_WIDTH: f32 = 0.0088;
/// Height of the card corner for card identification (Vertical)
pub(super) const TEMPLATE_HEIGHT: f32 = 0.0241;

/// Coordinate to the center of the 'New Game' button (Horizontal)
pub(super) const NEW_GAME_X: f32 = 0.9000;
/// Coordinate to the center of the 'New Game' button (Vertical)
pub(super) const NEW_GAME_Y: f32 = 0.9500;
