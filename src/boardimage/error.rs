//
// Copyright (C) 2021,2023  Florent "Wirefl0w" C.
//
// This file is part of shenzhen_io_solitaire_solver_bot
//
// shenzhen_io_solitaire_solver_bot is free software:
// you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>
//

//! Error handling for [`BoardImage`](super::BoardImage).

use std::fmt::{Display, Formatter};

use crate::game::Card;

/// Type alias for Result returned by [`BoardImage`](super::BoardImage).
pub type Result<T> = std::result::Result<T, Error>;

/// Custom error type for [`BoardImage`](super::BoardImage) error.
#[derive(Clone, Debug)]
pub enum Error {
    /// Encountered when trying to update the image of the [`BoardImage`](super::BoardImage)
    /// with an image with different dimensions
    DimensionsMismatch,
    /// An impossible card has been identified in a goal.
    /// Holds the [`Card`] and the goal index.
    ImpossibleGoal(Card, u8),
    /// An Card could not be identified in a place where there should be
    /// one.
    /// Holds the index of the pile and the card.
    UnidentifiableBoard { pile_idx: u8, card_idx: u8 },
    /// Card cannot be identified.
    /// Holds the maximum match score the card got along the minimum
    /// allowed score.
    UnidentifiableCard { score: f64, min_score: f64 },
}

impl std::error::Error for Error {}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::DimensionsMismatch =>
                write!(f, "Given image doesn't have same dimensions than initialization one"),
            Error::ImpossibleGoal(card, goal_idx) =>
                write!(f, "Impossible Card indentified in goal {}: {:?}", goal_idx, card),
            Error::UnidentifiableBoard { pile_idx, card_idx } =>
                write!(f, "Wasn't able to identify Board. Empty card found on pile {}, offset {}",
                       pile_idx, card_idx),
            Error::UnidentifiableCard { score, min_score } =>
                write!(f, "Unidentifiable Card: score {} < {} minimum score",
                       score, min_score),
        }
    }
}
