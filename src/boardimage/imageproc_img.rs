//
// Copyright (C) 2021,2023  Florent "Wirefl0w" C.
//
// This file is part of shenzhen_io_solitaire_solver_bot
//
// shenzhen_io_solitaire_solver_bot is free software:
// you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>
//

//! Module providing image recognition
//! routines through the Rust [`image`] & [`imageproc`] crates.

use image::{open, load_from_memory, GrayImage};
use image::imageops::{resize, FilterType};
use image::imageops::grayscale;
use image::SubImage;

use imageproc::template_matching::{match_template, MatchTemplateMethod, find_extremes};

/// Wrapper around [`GrayImage`] providing necessary manipulation &
/// detection functions for [`BoardImage`](super::BoardImage).
#[derive(Clone)]
pub(super) struct ImageProcImg {
    image: GrayImage,
}

impl ImageProcImg {
    /// Loads the image from image data.
    pub fn load(img: Vec<u8>) -> Self {
        let image = load_from_memory(&img).unwrap().into_luma8();

        ImageProcImg {
            image
        }
    }

    /// Opens the image from a file.
    pub fn open(filepath: &str) -> Self {
        let image = open(filepath).unwrap().into_luma8();

        ImageProcImg {
            image
        }
    }

    /// Returns a new image containing only the rect of the given
    /// dimensions and which top-left corner is at `(x, y)`
    pub fn sub_img(&self, x: u32, y: u32, width: u32, height: u32) -> Self {
        let image = SubImage::new(&self.image, x, y, width, height).to_image();

        ImageProcImg {
            image: grayscale(&image),
        }
    }

    /// Returns the size `(width, height)` of the Image object in pixel.
    pub fn size(&self) -> (u32, u32) {
        (self.image.width(), self.image.height())
    }

    /// Resizes the image to the given dimensions.
    pub fn resize(&mut self, width: u32, height: u32) {
        self.image = resize(&self.image, width, height, FilterType::Gaussian);
    }

    /// Performs template matching against `other`
    /// and returns the best matching score.
    pub fn match_template(&self, other: &Self) -> f64 {
        let res = match_template(&self.image, &other.image, MatchTemplateMethod::CrossCorrelationNormalized);

        find_extremes(&res).max_value.into()
    }

}


/// Drawing
use imageproc::drawing::{draw_hollow_rect_mut, draw_filled_circle_mut};
use imageproc::rect::Rect;
use image::{Rgb, RgbImage};
use image::buffer::ConvertBuffer;

const RED:   Rgb<u8> = Rgb([255, 0, 0]);
const GREEN: Rgb<u8> = Rgb([0, 255, 0]);

/// A wrapper around a [`RgbImage`] which can be used to draw
/// the zones for debugging purposes.
pub(super) struct ImageProcDrawImg {
    image: RgbImage,
}

impl ImageProcDrawImg {
    /// Create an new [`ImageProcDrawImg`] from the given [`ImageProcImg`].
    pub fn from_img(img: &ImageProcImg) -> Self {
        let image = img.image.convert();

        ImageProcDrawImg {
            image
        }
    }

    /// Saves the image at the given filepath.
    pub fn save(&self, filepath: &str) {
        self.image.save(filepath).unwrap();
    }

    /// Draw a red rectangle of the given size which top left corner is
    /// at given coordinates.
    pub fn draw_detection_zone(&mut self, x: u32, y: u32, width: u32, height: u32) {
        draw_hollow_rect_mut(&mut self.image,
                             Rect::at(x as i32, y as i32).of_size(width, height),
                             RED);
    }

    /// Draw a filled green circle with the given `(x, y)` on the image.
    pub fn draw_click_zone(&mut self, x: u32, y: u32) {
        draw_filled_circle_mut(&mut self.image, (x as i32, y as i32), 5, GREEN);
    }
}

