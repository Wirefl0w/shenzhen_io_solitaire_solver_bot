//
// Copyright (C) 2021, 2023-2024  Florent "Wirefl0w" C.
//
// This file is part of shenzhen_io_solitaire_solver_bot
//
// shenzhen_io_solitaire_solver_bot is free software:
// you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>
//

//! Module providing image recognition and giving clickable area coordinates.
//!
//! This module provides the `BoardImage` struct. It loads a screenshot from the
//! game and is able to identify the `Board`, as well as giving pixel coordinates
//! of Cards, Goals, and other interesting areas of the game board.
//!
//! The module makes use of `opencv` to provide Card detection through
//! template matching (using cross corelation). This might certainly change
//! as it is quite overkill and complexify a lot the compilation process. \
//! Other libraries such as `imageproc` might soon be use instead.

use std::collections::HashMap;

use crate::game::{Card, CardColor, CardValue};
use crate::game::{Board, N_PILES, N_GOALS};

mod error;
mod dimensions;

mod imageproc_img;

use error::{Error, Result};
use dimensions::*;

use imageproc_img::{ImageProcImg, ImageProcDrawImg};

type Image = ImageProcImg;
type DrawImage = ImageProcDrawImg;

/// Minimum score for card matching
const MATCH_SCORE: f64 = 0.99;

/// Struct providing image detection functionalities
/// and area coordinates
///
/// Contains the image of the board
/// and provides methods to analyze and retrieve the
/// Board as well as coordinates to intersting clickable areas.
#[derive(Clone)]
pub struct BoardImage {
    width: f32,
    height: f32,
    offset_x: u32,
    offset_y: u32,

    image: Image,
    templates: Vec<(Card, Image)>,
    corner_dim: (u32, u32)
}

impl BoardImage {
    /// Initializes the object with the given png data.
    ///
    /// Loads the given image and cards templates. \
    /// This function should be called once. If you need
    /// to identify other boards with same image dimensions
    /// you should call `update_image`.
    pub fn new(img_data: Vec<u8>) -> Self {
        // Load image and store size
        let image = Image::load(img_data);
        let (image, offset_x, offset_y) = Self::crop_game_screen(image);
        let (width, height) = image.size();
        let (width, height) = (width as f32, height as f32);

        // Load associated templates with right dimensions
        let mut templates = Vec::new();
        for card in Card::all() {
            let filepath = format!("cards_templates/{}.png", card);
            let template = Image::open(&filepath);
            templates.push((card, template));
        }

        // Resize templates if needed
        //   Compute needed size and effective size
        let (t_w, t_h) = templates[0].1.size();
        let dst_w = (width * TEMPLATE_WIDTH).round() as u32;
        let dst_h = (height * TEMPLATE_HEIGHT).round() as u32;

        if t_w != dst_w || t_h != dst_h {
            println!("[WARNING] The given image isn't the same size \
                as the one used to extract templates.
                The templates will be resized accordingly but it might cause some inaccurate results
                (You will also probably need to change the matching score threshold)");
            for (_, template_img) in &mut templates {
                // Resize and update the Image object
                template_img.resize(dst_w, dst_h);
            }
        }

        // Dimensions of the corner of a Card used to identify it
        let corner_w = (width * CARD_WIDTH / 3.0).round() as u32;
        let corner_h = (height * CARD_HEIGHT / 7.0).round() as u32;
        let corner_dim = (corner_w, corner_h);

        BoardImage {
                width,
                height,
                offset_x,
                offset_y,

                image,
                templates,
                corner_dim,
            }
    }

    /// Update the board image with a new image of the **same size**
    /// without reloading/resizing templates
    pub fn update_image(&mut self, img_data: Vec<u8>) -> Result<()> {
        let image = Image::load(img_data);
        let (image, offset_x, offset_y) = Self::crop_game_screen(image);

        let (width, height) = image.size();

        if self.width as u32 != width || self.height as u32 != height ||
                offset_x != self.offset_x || offset_y != self.offset_y {
            Err(Error::DimensionsMismatch)
        } else {
            self.image = image;
            Ok(())
        }
    }

    /// Crop the border to only keep the Solitaire game screen which
    /// is contained inside the main game screen.
    ///
    /// It returns the cropped image in grayscale as well
    /// as its offset coordinates (x, y) in the full screen.
    fn crop_game_screen(img: Image) -> (Image, u32, u32) {
        let (width, height) = img.size();
        let offset_x = (width as f32 * GAME_SCREEN_LEFT_BORDER) as u32;
        let offset_y = (height as f32 * GAME_SCREEN_TOP_BORDER) as u32;

        let min_x = offset_x;
        let max_x = width - offset_x;

        let min_y = offset_y;
        let max_y = height - offset_y;

        let width = max_x - min_x;
        let height = max_y - min_y;

        let new_img = img.sub_img(min_x, min_y, width, height);

        (new_img, offset_x, offset_y)
    }

    /// Gives the pixels coordinates to the center
    /// of the **New Game Button**.
    pub fn new_game_coords(&self) -> (u32, u32) {
        let x = (self.width * NEW_GAME_X).round() as u32;
        let y = (self.height * NEW_GAME_Y).round() as u32;
        (x + self.offset_x, y + self.offset_y)
    }

    /// Gives the pixels coordinates to the center
    /// of the given **space** in the **screen**.
    pub fn space_coords(&self, s_idx: u8) -> (u32, u32) {
        let x = (self.width * (LEFT_TO_SPACE + s_idx as f32 * SPACE_TO_SPACE)).round() as u32;
        let y = (self.height * TOP_TO_SPACES).round() as u32;
        (x + self.offset_x, y + self.offset_y)
    }

    /// Gives the pixels coordinates to the center
    /// of the given **dragon circle** in the **screen**.
    pub fn circle_coords(&self, c_idx: u8) -> (u32, u32) {
        let x = (self.width * LEFT_TO_CIRCLES).round() as u32;
        let y = (self.height * (TOP_TO_CIRCLE + c_idx as f32 * CIRCLE_TO_CIRCLE)).round() as u32;
        (x + self.offset_x, y + self.offset_y)
    }

    /// Gives the pixels coordinates to the center
    /// of the given **goal** in the **screen**.
    pub fn goal_coords(&self, g_idx: u8) -> (u32, u32) {
        let x = (self.width * (LEFT_TO_GOAL + g_idx as f32 * GOAL_TO_GOAL)).round() as u32;
        let y = (self.height * TOP_TO_GOALS).round() as u32;
        (x + self.offset_x, y + self.offset_y)
    }

    /// Gives the coordinates of the **top left corner**
    /// of the given card on the given **goal** in the **inner image**.
    fn goal_corner_coords_inner(&self, g_idx: u8) -> (u32, u32) {
        let (mut x, mut y) = self.goal_coords(g_idx);
        x -= self.offset_x + (self.width * CARD_WIDTH / 2.0).round() as u32;
        y -= self.offset_y + (self.height * CARD_HEIGHT / 2.0).round() as u32;
        (x, y)
    }

    /// Returns an image containing the value (on the top-left corner)
    /// of the given **goal**.
    fn goal_corner_img(&self, g_idx: u8) -> Image {
        let (w, h) = self.corner_dim;
        let (x, y) = self.goal_corner_coords_inner(g_idx);

        self.image.sub_img(x, y, w, h)
    }

    /// Returns an image containing the value (on the top-left corner)
    /// of the **flower goal**.
    fn flower_corner_img(&self) -> Image {
        let (w, h) = self.corner_dim;
        let x = (self.width * (LEFT_TO_FLOWER - CARD_WIDTH / 2.0)).round() as u32;
        let y = (self.height * (TOP_TO_FLOWER - CARD_HEIGHT / 2.0)).round() as u32;

        self.image.sub_img(x, y, w, h)
    }

    /// Gives the pixels coordinate to the **top of the given card on a pile**.
    /// The pixel belons belongs to the given card **even if more
    /// cards are over it**.
    pub fn pile_click_coords(&self, p_idx: u8, c_idx: u8) -> (u32, u32) {
        let (x, mut y) = self.pile_coords_inner(p_idx, c_idx);
        y -= (self.height * (CARD_HEIGHT - CARD_OFFSET) / 2.0).round() as u32;
        (x + self.offset_x, y + self.offset_y)
    }

    /// Gives the pixels coordinates of the **center**
    /// of the given **card** on the given **pile** in the **inner image**.
    fn pile_coords_inner(&self, p_idx: u8, c_idx: u8) -> (u32, u32) {
        let x = (self.width * (LEFT_TO_PILE + p_idx as f32 * PILE_TO_PILE)).round() as u32;
        let y = (self.height * (TOP_TO_PILES + c_idx as f32 * CARD_OFFSET)).round() as u32;
        (x, y)
    }

    /// Gives the coordinates of the **top left corner**
    /// of the given **card** on the given **pile** in the **inner image**.
    fn pile_corner_coords_inner(&self, p_idx: u8, c_idx: u8) -> (u32, u32) {
        let (mut x, mut y) = self.pile_coords_inner(p_idx, c_idx);
        x -= (self.width * CARD_WIDTH / 2.0).round() as u32;
        y -= (self.height * CARD_HEIGHT / 2.0).round() as u32;
        (x, y)
    }

    /// Returns an image containing the value (on the top-left corner)
    /// of the given **card** on the given **pile**.
    fn pile_corner_img(&self, p_idx: u8, c_idx: u8) -> Image {
        let (w, h) = self.corner_dim;
        let (x, y) = self.pile_corner_coords_inner(p_idx, c_idx);

        self.image.sub_img(x, y, w, h)
    }


    /// Check if there are already card on the goal stacks.
    ///
    /// Returns a `Vec` of `Cards` in a **appropriate order** to be
    /// automoved the same way as in the image.
    /// The `Cards` are ordered by number (1 on top) and color
    /// order matches the goal order.
    fn detect_goals(&self) -> Result<Vec<Card>> {
        // Order is quite important on this function
        // and things can get confusing regarding the
        // logic used.
        // First, a Vec of Vec of Card is created (one for each color)
        // to list all moved Cards
        // Then, with this Vec of Vec, a Vec is built by taking all number 1
        // in the right order of color, then all number 2 and so on.
        // Then, the whole list is reversed (so 1 goes on top)
        // That way, the Vec<Vec<Cards>> should be arranged so:
        //  - Vec<Cards> have number 1 on top
        //  - First goal color is on top

        // This vec will contain one pile for each color
        let mut moved_cards_colors = Vec::<Vec<Card>>::new();

        // Get only numbers and flower templates
        let mut templates: Vec<(Card, &Image)> = self.templates.iter()
                .filter_map(|(c, m)| match c.value {
                    CardValue::Dragon => None,
                    _ => Some((*c, m)),
                    }
                ).collect();

        // Check the flower
        let corner = self.flower_corner_img();
        //let flower_templ = templates.iter().find(|(c, _)| *c == Card::flower()).unwrap().clone();
        let flower_templ = templates.last().unwrap(); // The flower is the last template

        let flower_cleared = self.identify_card(corner, &vec!(*flower_templ)).is_ok();

        // Remove flower from templates
        //templates.retain(|(c, _)| *c != Card::flower());
        templates.pop().unwrap(); // Flower is the last element

        // Check goals (in reverse order)
        for i in 1..=N_GOALS {
            let g_idx = (N_GOALS - i) as u8;
            let corner = self.goal_corner_img(g_idx);
            // Try to identify Cards on Goals
            let card = self.identify_card(corner, &templates);
            if let Ok(card) = card {
                match &card.value {
                    CardValue::Number(num) => {
                        // Add every Card from 1 to this one to the list
                        // 1st on top
                        let mut pile = Vec::<Card>::with_capacity(*num as usize);
                        for card_num in 1..=*num {
                            let moved_card = Card::number(card.color, card_num);
                            pile.insert(0, moved_card);
                        }
                        moved_cards_colors.insert(0, pile); // First Goal on top

                        // Remove all Card of this color from templates
                        templates.retain(|(c, _)| c.color != card.color);
                    },
                    _ => {
                        return Err(Error::ImpossibleGoal(card, g_idx));
                    }
                };
            }
        }

        // The Vec of moved Cards
        let mut moved_cards = Vec::<Card>::new();

        // Take flower first
        if flower_cleared {
            moved_cards.push(Card::flower());
        }

        let mut not_empty = !moved_cards_colors.is_empty();
        // Building the Vec by taking the firsts number of the first goal first
        while not_empty {
            not_empty = false;
            for p in &mut moved_cards_colors {
                if !p.is_empty() {
                    not_empty = true;
                    moved_cards.push(p.pop().unwrap());
                }
            }
        }

        // Reversing it so first things are on top
        moved_cards.reverse();

        Ok(moved_cards)
    }

    /// Draws lines on places where detection is made and
    /// saves the generated image at the given `path`.
    ///
    /// - Red zones indicates detection zones
    /// - Green zones indicates click zones
    ///
    /// This function is useful to tweak dimensions.
    pub fn display_detection_zones(&self, filepath: &str) {
        let mut display_img = DrawImage::from_img(&self.image);
        let (w, h) = self.corner_dim;

        // Display goals
        for g_idx in 0..N_GOALS as u8 {
            let (x, y) = self.goal_corner_coords_inner(g_idx);
            display_img.draw_detection_zone(x, y, w, h);
        }

        // Display card corners
        for p_idx in 0..N_PILES as u8 {
            for c_idx in 0u8..5 {
                let (x, y) = self.pile_corner_coords_inner(p_idx, c_idx);
                display_img.draw_detection_zone(x, y, w, h);
            }
        }

        // Display Spaces
        for s_idx in 0u8..3 {
            let (x, y) = self.space_coords(s_idx);
            let x = x - self.offset_x;
            let y = y - self.offset_y;

            display_img.draw_click_zone(x, y);
        }

        // Display circles
        for c_idx in 0u8..3 {
            let (x, y) = self.circle_coords(c_idx);
            let x = x - self.offset_x;
            let y = y - self.offset_y;

            display_img.draw_click_zone(x, y);
        }

        // New game button
        let (x, y) = self.new_game_coords();
        let x = x - self.offset_x;
        let y = y - self.offset_y;
        display_img.draw_click_zone(x, y);

        display_img.save(filepath);
    }


    /// Identify the board from game image.
    pub fn identify_board(&self) -> Result<Board> {
        // This Vec will be poped each time a Card is found to avoid duplicates
        let mut templates: Vec<(Card, &Image)> =
                self.templates.iter().map(|(c, m)| (*c, m)).collect();
        // To count the number of each found dragons
        let mut dragons: HashMap<Card, u8> = HashMap::new();
        dragons.insert(Card::dragon(CardColor::Red), 4);
        dragons.insert(Card::dragon(CardColor::Green), 4);
        dragons.insert(Card::dragon(CardColor::Black), 4);

        // If any automoves have been done at start, detection errors on piles will be ignored
        // (as long as they are not more than auto moved cards).
        // Before creating the board, the moved cards will be appended to piles
        // (one pile for each color), so tha when creating the board, auto moves
        // will automatically be applied, clearing these cards
        let mut moved_cards = self.detect_goals()?;


        // Remove all moved_cards from templates
        templates.retain(|(c, _)| !moved_cards.contains(c));

        // Identify Cards on the pile
        let mut piles: [Vec<Card>; N_PILES] = Default::default();

        let mut n_empty = 0; // Number of ignored unmatched card
        for p_idx in 0..N_PILES as u8{
            for c_idx in 0u8..5 {
                let corner = self.pile_corner_img(p_idx, c_idx);
                let card = self.identify_card(corner, &templates);
                match card {
                    Ok(card) => {
                        // Remove card from templates to avoid duplicates
                        // (If it's a dragon, just check if there are still some, before)
                        let remove_card = match &card.value {
                            CardValue::Dragon => {
                                let n_dragons = dragons.get_mut(&card).unwrap();
                                *n_dragons -= 1;
                                // Remove card from the template if there is no more
                                *n_dragons == 0
                            },
                            _ => true,
                        };
                        if remove_card {
                            let needle = templates.iter().position(|(c, _)| *c == card).unwrap();
                            templates.remove(needle);
                        }

                        piles[p_idx as usize].push(card);
                    },
                    Err(_) => {
                        n_empty += 1;
                        if n_empty > moved_cards.len() {
                            return Err(Error::UnidentifiableBoard {
                                pile_idx: p_idx,
                                card_idx: c_idx
                            });
                        }
                    }
                };
            }
        }

        // Check if some Cards have been automove at start
        if !moved_cards.is_empty() {
            // Add the moved_card to a pile
            piles[0].append(&mut moved_cards);
        }

        Ok(Board::new(piles))
    }

    /// Try to identify the given card corner with one of
    /// the given templates.
    fn identify_card(&self, corner: Image, templates: &Vec<(Card, &Image)>) -> Result<Card> {
        let mut score: f64 = 0.0;
        let mut matched_card: Option<Card> = None;

        for (card, template) in templates {
            // Match the template
            let match_score = corner.match_template(template);

            if match_score > score {
                score = match_score;
                matched_card = Some(*card);
            }
        }

        if score < MATCH_SCORE {
            Err(Error::UnidentifiableCard { score, min_score: MATCH_SCORE })
        } else {
            Ok(matched_card.unwrap())
        }
    }
}
