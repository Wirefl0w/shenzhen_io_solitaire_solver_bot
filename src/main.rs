//
// Copyright (C) 2021,2023  Florent "Wirefl0w" C.
//
// This file is part of shenzhen_io_solitaire_solver_bot
//
// shenzhen_io_solitaire_solver_bot is free software:
// you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>
//

use std::error::Error;
use std::io::{stdin, stdout, Write};
use std::thread::sleep;
use std::time::Duration;
use std::process::exit;

use screenshots::Screen;
use clap::{App, Arg};

mod bot;
use bot::GameBot;

mod boardimage;
use boardimage::BoardImage;

mod game;
use game::find_solution;

/// The filepath where to save the image with detection zones.
static SCREEN_ZONES_PATH: &str = "./sio_bot_zones.png";

/// Flushing version of `print` maccro.
macro_rules! print {
    ( $($t:tt)* ) => {
        {
            let mut s = stdout();
            write!(s, $($t)*).unwrap();
            s.flush().unwrap();
        }
    };
}

fn main() -> Result<(), Box<dyn Error>> {
    let args = Args::parse();

    println!("    ===  Shenzhen I/O Solitaire Solver Bot  ===\n");

    let mut n_games = args.n_games;

    let mut bot = GameBot::new();
    let mut img: Option<BoardImage> = None;

    let screens = Screen::all()?;

    if args.screen as usize > screens.len() {
        println!("Error: Only {} screens are available", screens.len());
        exit(1);
    }

    let screen = screens[args.screen as usize];

    if args.screen_zones {
        print!("[*] Taking Screenshot ");
        let image = screen.capture()?.to_png()?;
        println!("- Done");

        let board_img = BoardImage::new(image);

        print!("[*] Saving image ");
        board_img.display_detection_zones(SCREEN_ZONES_PATH);
        println!("- Done");

        print!("[*] Identify board ");
        let board = board_img.identify_board()?;
        println!("- Done");
        println!("{}", board);

        return Ok(());
    }

    if !args.start {
        // Some advices displayed for the user
        println!("The game window needs to be focused for the bot to proceed \
                    Once ready, you will have a 3 seconds countdown, then \
                    The bot will click on the top left of the screen to catch \
                    the game window focus");
        pause();
    }

    // Countdown before start
    for i in 0..3 {
        print!("{}...", 3-i);
        sleep(Duration::from_secs(1));
    }
    println!("0!");

    // Catch focus before start
    bot.catch_focus()?;

    // Running the bot
    while n_games > 0 {
        println!("\n{} Game(s) left to solve", n_games);

        if img.is_some() {
            // Click new game (if not the first time running)
            println!("Starting new game");
            bot.click_new_game()?;
            // Wait for cards to be distributed
            sleep(Duration::from_secs(6));
        }

        // Take a screenshot
        println!("Taking screenshot");
        let image = screen.capture()?.to_png()?;

        // Load BoardImage from screenshot
        img = match img {
            Some(mut im) => { // If BoardImage already initialized
                // Just update it
                im.update_image(image)?;
                Some(im)
            },
            // Otherwise, initialize it
            None => {
                let im = BoardImage::new(image);
                // Initialize bot once with BoardImage
                bot.init(im.clone());
                Some(im)
            }
        };

        // Identifying Board
        println!("Identifying board");
        let board = match img.as_ref().unwrap().identify_board() {
            Ok(b) => b,
            Err(_) => {
                println!("Failed to identify board");
                continue;
            }
        };

        // Solve the board
        println!("Finding solution...");
        if let Some(sol) = find_solution(board.clone(), args.timeout) {
            // Give Board to bot before start
            bot.start(board.clone());

            // Replay the solution
            let n_move = sol.len();
            for (i, m) in sol.iter().enumerate() {
                progress(i+1, n_move);
                bot.play_move(*m)?;
            }
            println!("\nGame solved");
            // Wait a little bit before starting again
            sleep(Duration::from_secs(3));
        } else {
            // Board not solvable, start new game
            continue;
        }

        n_games -= 1;
    }

    println!("\nBot is over");
    println!("Exiting...");

    Ok(())
}

/// Displays a message and wait for user to press Enter.
fn pause() {
    print!("Press Enter to continue...");
    stdin().read_line(&mut String::new()).unwrap();
}

/// Display a progress bar
/// considering `i` over `n` elements are complete.
fn progress(i: usize, n: usize) {
    let bar_size = 20.0;
    let p = i as f32 / n as f32;
    let fill = (p * bar_size) as usize;
    print!("\rPlaying Solution |{}{}| - {} / {}  ",
            "#".repeat(fill),
            " ".repeat(bar_size as usize - fill),
            i,
            n
        );
}

/// Structure holding program parameters from cli arguments.
struct Args {
    start: bool,
    n_games: u8,
    screen: u8,
    screen_zones: bool,
    timeout: Option<Duration>,
}

impl Args {
    /// Parse CLI arguments and fills in an Args struct containing values
    fn parse() -> Self {
        // Create the App containing all paramters description
        let arg = App::new("Shenzhen I/O Solitaire Solver Bot")
                .version("0.9.2")
                .author("Wirefl0w")
                .about("A Rust Solver Bot for the Shenzhen I/O Solitaire game")
                .arg(Arg::with_name("nb solves")
                        .takes_value(true)
                        .default_value("1")
                        .help("The number of game the solver will solve. (Max 255)"))
                .arg(Arg::with_name("start")
                        .long("start")
                        .takes_value(false)
                        .help("Start the solver without asking to press Enter"))
                .arg(Arg::with_name("timeout")
                        .short("t")
                        .long("timeout")
                        .takes_value(true)
                        .default_value("5000")
                        .help("The timeout (in ms) at which the solver will consider the game not solvable."))
                .arg(Arg::with_name("screen_zones")
                        .short("z")
                        .long("show_zones")
                        .takes_value(false)
                        .help(&format!("Without playing, displays detected board and saves a png \
                              image with the detection/click zones drawed at '{}'.", SCREEN_ZONES_PATH)))
                .arg(Arg::with_name("screen")
                        .short("s")
                        .long("screen")
                        .takes_value(true)
                        .default_value("0")
                        .help("The screen to take a screenshot from (if multiple monitors are connected"))
                .get_matches();


        // Extracting the values
        let start = arg.is_present("start");
        let screen_zones = arg.is_present("screen_zones");
        let n_games: u8 = match arg.value_of("nb solves").unwrap().parse() {
                Ok(val) => val,
                Err(_)  => {
                    println!("Error parsing argument 'nb solves', integer expected.");
                    exit(1);
                }
            };
        let timeout: u64 = match arg.value_of("timeout").unwrap().parse() {
                Ok(val) => val,
                Err(_)  => {
                    println!("Error parsing argument 'timeout', integer expected");
                    exit(1);
                }
            };
        let timeout = Some(Duration::from_millis(timeout));

        let screen: u8 = match arg.value_of("screen").unwrap().parse() {
            Ok(val) => val,
            Err(_)  => {
                println!("Error parsing argument 'screen', integer expected.");
                exit(1);
            },
        };

        Args {
            start,
            n_games,
            screen,
            screen_zones,
            timeout,
        }
    }
}
